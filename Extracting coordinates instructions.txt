1.
Open Atom. Drag the files Bleak House places.xml into it.

2.
In Atom, Ctrl+Shift+P 

2b. Search soft, the second option should be Toggle soft wrap. Select this. You should now see the entire text in your Atom editor window.

3. 
Use the following regular expressions to extract the place information (DON'T FORGET TO SELECT THE REGULAR EXPRESSIONS BUTTON):

To get them all on one line:

Find: 
(<place[^>]+>[^<]+</place>)
Replace all: 
\n#$1\n

To delete the rest of the text: 

Find:
^[^#].+\n
Replace with nothing

To make them tsv:

Find: 
#<place type="([^"]+)" coordinates="([^"]+)">([^>]+)</place>
Replace: 
$1\t$2\t$3

Save as .tsv 


CONTINUE THE REST AT HOME


4. 
Open in Excel
Insert row at the top of the worksheet and label the columns: Type, Coordinates, Place name
Correct apostrophe in Lincoln's Inn Hall

5. Save as Excel Workbook 

6. Sign into Google and go to maps.google.co.uk > Your Places > Maps > See All Your Maps

7. Create Map

8. Import the Excel file

9. Choose to position your placemarks using the coordinates column (latitude,longitude)

10. Choose to title your markers using place name column

11. Under Individual Styles > Group Places by Type and set labels by Place name




